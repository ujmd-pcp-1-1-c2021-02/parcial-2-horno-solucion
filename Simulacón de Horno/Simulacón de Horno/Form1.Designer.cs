﻿
namespace Simulacón_de_Horno
{
    partial class frmHorno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHorno));
            this.pbxTermómetro = new System.Windows.Forms.PictureBox();
            this.pbxHornoON = new System.Windows.Forms.PictureBox();
            this.pbxHornoOFF = new System.Windows.Forms.PictureBox();
            this.panelTermómetro = new System.Windows.Forms.Panel();
            this.panelMercurio = new System.Windows.Forms.Panel();
            this.btnON = new System.Windows.Forms.Button();
            this.btnOFF = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nudTemperatura = new System.Windows.Forms.NumericUpDown();
            this.tmrON = new System.Windows.Forms.Timer(this.components);
            this.tmrOFF = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxTermómetro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoOFF)).BeginInit();
            this.panelTermómetro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxTermómetro
            // 
            this.pbxTermómetro.Image = global::Simulacón_de_Horno.Properties.Resources.Termómetro_26_180_Celcius;
            this.pbxTermómetro.Location = new System.Drawing.Point(231, 8);
            this.pbxTermómetro.Name = "pbxTermómetro";
            this.pbxTermómetro.Size = new System.Drawing.Size(100, 423);
            this.pbxTermómetro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxTermómetro.TabIndex = 2;
            this.pbxTermómetro.TabStop = false;
            // 
            // pbxHornoON
            // 
            this.pbxHornoON.Image = global::Simulacón_de_Horno.Properties.Resources.Ahumador_encendido_GIF;
            this.pbxHornoON.Location = new System.Drawing.Point(21, 87);
            this.pbxHornoON.Name = "pbxHornoON";
            this.pbxHornoON.Size = new System.Drawing.Size(177, 168);
            this.pbxHornoON.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoON.TabIndex = 1;
            this.pbxHornoON.TabStop = false;
            this.pbxHornoON.Visible = false;
            // 
            // pbxHornoOFF
            // 
            this.pbxHornoOFF.Image = global::Simulacón_de_Horno.Properties.Resources.Ahumador_apagado;
            this.pbxHornoOFF.Location = new System.Drawing.Point(21, 87);
            this.pbxHornoOFF.Name = "pbxHornoOFF";
            this.pbxHornoOFF.Size = new System.Drawing.Size(177, 168);
            this.pbxHornoOFF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoOFF.TabIndex = 0;
            this.pbxHornoOFF.TabStop = false;
            // 
            // panelTermómetro
            // 
            this.panelTermómetro.BackColor = System.Drawing.Color.White;
            this.panelTermómetro.Controls.Add(this.panelMercurio);
            this.panelTermómetro.Location = new System.Drawing.Point(266, 53);
            this.panelTermómetro.Name = "panelTermómetro";
            this.panelTermómetro.Size = new System.Drawing.Size(12, 300);
            this.panelTermómetro.TabIndex = 3;
            // 
            // panelMercurio
            // 
            this.panelMercurio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(11)))), ((int)(((byte)(21)))));
            this.panelMercurio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMercurio.Location = new System.Drawing.Point(0, 217);
            this.panelMercurio.Name = "panelMercurio";
            this.panelMercurio.Size = new System.Drawing.Size(12, 83);
            this.panelMercurio.TabIndex = 0;
            // 
            // btnON
            // 
            this.btnON.Location = new System.Drawing.Point(21, 267);
            this.btnON.Name = "btnON";
            this.btnON.Size = new System.Drawing.Size(88, 23);
            this.btnON.TabIndex = 4;
            this.btnON.Text = "Encender";
            this.btnON.UseVisualStyleBackColor = true;
            this.btnON.Click += new System.EventHandler(this.btnON_Click);
            // 
            // btnOFF
            // 
            this.btnOFF.Location = new System.Drawing.Point(115, 267);
            this.btnOFF.Name = "btnOFF";
            this.btnOFF.Size = new System.Drawing.Size(87, 23);
            this.btnOFF.TabIndex = 5;
            this.btnOFF.Text = "Apagar";
            this.btnOFF.UseVisualStyleBackColor = true;
            this.btnOFF.Click += new System.EventHandler(this.btnOFF_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 304);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Temperatura (°C):";
            // 
            // nudTemperatura
            // 
            this.nudTemperatura.Location = new System.Drawing.Point(123, 302);
            this.nudTemperatura.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.nudTemperatura.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.Name = "nudTemperatura";
            this.nudTemperatura.Size = new System.Drawing.Size(51, 20);
            this.nudTemperatura.TabIndex = 7;
            this.nudTemperatura.Value = new decimal(new int[] {
            27,
            0,
            0,
            0});
            this.nudTemperatura.ValueChanged += new System.EventHandler(this.nudTemperatura_ValueChanged);
            // 
            // tmrON
            // 
            this.tmrON.Interval = 50;
            this.tmrON.Tick += new System.EventHandler(this.tmrON_Tick);
            // 
            // tmrOFF
            // 
            this.tmrOFF.Tick += new System.EventHandler(this.tmrOFF_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Red;
            this.pictureBox1.Location = new System.Drawing.Point(337, 357);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 15);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox2.Location = new System.Drawing.Point(337, 318);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 15);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox4.Location = new System.Drawing.Point(337, 240);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(27, 15);
            this.pictureBox4.TabIndex = 8;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox5.Location = new System.Drawing.Point(337, 201);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(27, 15);
            this.pictureBox5.TabIndex = 8;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox6.Location = new System.Drawing.Point(337, 162);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 15);
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox7.Location = new System.Drawing.Point(337, 123);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(27, 15);
            this.pictureBox7.TabIndex = 8;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox8.Location = new System.Drawing.Point(337, 84);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(27, 15);
            this.pictureBox8.TabIndex = 8;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox9.Location = new System.Drawing.Point(337, 45);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(27, 15);
            this.pictureBox9.TabIndex = 8;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox3.Location = new System.Drawing.Point(337, 279);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 15);
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // frmHorno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 443);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.nudTemperatura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOFF);
            this.Controls.Add(this.btnON);
            this.Controls.Add(this.panelTermómetro);
            this.Controls.Add(this.pbxTermómetro);
            this.Controls.Add(this.pbxHornoON);
            this.Controls.Add(this.pbxHornoOFF);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmHorno";
            this.Text = "Simulador de Horno";
            this.Load += new System.EventHandler(this.frmHorno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxTermómetro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoOFF)).EndInit();
            this.panelTermómetro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxHornoOFF;
        private System.Windows.Forms.PictureBox pbxHornoON;
        private System.Windows.Forms.PictureBox pbxTermómetro;
        private System.Windows.Forms.Panel panelTermómetro;
        private System.Windows.Forms.Panel panelMercurio;
        private System.Windows.Forms.Button btnON;
        private System.Windows.Forms.Button btnOFF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudTemperatura;
        private System.Windows.Forms.Timer tmrON;
        private System.Windows.Forms.Timer tmrOFF;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}

