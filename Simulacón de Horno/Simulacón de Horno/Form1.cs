﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulacón_de_Horno
{
    public partial class frmHorno : Form
    {
        public frmHorno()
        {
            InitializeComponent();
        }

        private void frmHorno_Load(object sender, EventArgs e)
        {
            nudTemperatura_ValueChanged(sender, e);
        }

        private void nudTemperatura_ValueChanged(object sender, EventArgs e)
        {
            /*
             * Temperatura  26°C --->  Altura = 0
             * Temperatura 180°C --->  Altura = PanelTermómetro.Height;
             * 
             *    (Temperatura - 26)    ---> Height
             *        180 - 26          ---> panelTermómetro.Height
             */

            panelMercurio.Height = (int) (nudTemperatura.Value - 26) * panelTermómetro.Height / (180 - 26);


            pictureBox2.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox3.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox4.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox5.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox6.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox7.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox8.BackColor = Color.FromArgb(64, 0, 0);
            pictureBox9.BackColor = Color.FromArgb(64, 0, 0);

            if (nudTemperatura.Value >= 040) pictureBox2.BackColor = Color.Red;
            if (nudTemperatura.Value >= 060) pictureBox3.BackColor = Color.Red;
            if (nudTemperatura.Value >= 080) pictureBox4.BackColor = Color.Red;
            if (nudTemperatura.Value >= 100) pictureBox5.BackColor = Color.Red;
            if (nudTemperatura.Value >= 120) pictureBox6.BackColor = Color.Red;
            if (nudTemperatura.Value >= 140) pictureBox7.BackColor = Color.Red;
            if (nudTemperatura.Value >= 160) pictureBox8.BackColor = Color.Red;
            if (nudTemperatura.Value >= 180) pictureBox9.BackColor = Color.Red;
        }

        private void tmrON_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value < 180)
            {
                nudTemperatura.Value++;
            }
        }

        private void tmrOFF_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value > 26)
            {
                nudTemperatura.Value--;
            }
        }

        private void btnON_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = true;
            tmrOFF.Enabled = false;
            pbxHornoON.Visible = true;
        }

        private void btnOFF_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = false;
            tmrOFF.Enabled = true;
            pbxHornoON.Visible = false;
        }
    }
}
